package Service;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by USER on 14/7/2015.
 */
public class ShowData {
    public static JSONArray showReport() {
        Connection con;
        JSONArray list1 = new JSONArray();

        try {

            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB

            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/fbprofile", "root", "root");
            Statement st = con.createStatement();

            ResultSet Rs = st.executeQuery("SELECT personal_infomation.personal_id,first_name,last_name,email,gender,age,Page_name,Page_catagory FROM fbprofile.personal_infomation inner join fbprofile.personal_lifestyle ON personal_infomation.Personal_id = personal_lifestyle.Personal_id inner join fbprofile.facebook_page ON personal_lifestyle.Page_id = facebook_page.Page_id;");

            while (Rs.next()) {
                JSONObject obj = new JSONObject();

                obj.put("Id", Rs.getInt("personal_id"));
                obj.put("First_name", Rs.getString("first_name"));
                obj.put("Last_name", Rs.getString("last_name"));
                obj.put("Email", Rs.getString("email"));
                obj.put("Sex", Rs.getString("gender"));
                obj.put("Age", Rs.getString("age"));
                obj.put("catagory", Rs.getString("Page_catagory"));

                list1.put(obj);

            }
            Rs.close();
            st.close();
            con.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list1;
    }
}
