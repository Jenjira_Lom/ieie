package Resource;


import Service.ShowData;

import static spark.Spark.get;

/**
 * Created by USER on 14/7/2015.
 */
public class DataJSON {

    public static void main(String[] args) {

        get("/ListFB", (Request, response) -> {
            response.header("Cache-control", "no-cache, no-store");
            response.header("Pragma", "no-cache");
            response.header("Expires", "-1");

            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "POST");
            response.header("Access-Control-Allow-Headers", "Content-Type");
            response.header("Access-Control-Max-Age", "86400");
            return  ShowData.showReport();
        });

    }}
